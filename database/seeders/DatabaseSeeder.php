<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Cirilo Bucatcat Jr.',
            'username' => 'cirilo12',
            'password' => Hash::make('cirilo12')
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
