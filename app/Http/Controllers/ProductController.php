<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Create a new ProductController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwtauth');
    }

    public function get(Request $request) {
        return Product::where('user_id', $request->user_id)->paginate(5);
    }

    public function total(Request $request) {
        return response()->json(['total_products' => Product::all()->where('user_id', $request->user_id)->count()]);
    }

    public function outOfStock(Request $request) {
        return response()->json(['out_of_stock' => Product::where('user_id', $request->user_id)->where('stock', 0)->count()]);
    }

    public function store(Request $request) {
        
        $validate = Validator::make($request->all(), [
            'product.name'  => 'required',
            'product.price' => 'required',
            'product.stock'  => 'required',
            'product.category'  => 'required',
            
        ]);
        if ($validate->fails())
        {
            $message = $validate->errors()->getMessages();

            return response()->json([
                'success' => false,
                'messages' => $message 
            ], 200);
        }
        $product = Product::create([
            'user_id' => $request->user_id,
            'name' => $request->product['name'],
            'price' => 'NO PRICE',
            'stock' => $request->product['stock'],
            'category' => $request->product['category']
        ]);

        return response()->json(['success' => true, 'message' => 'Product Added!', 'product' => $product], 200);
    }

    public function delete(Request $request) {
        Product::where('id', $request->id)->where('user_id', $request->user_id)->delete();
        return response()->json([ 'message' => 'Product Deleted!', 'id' => $request->user_id],  200);
    }

    public function update(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'product.name'  => 'required',
            'product.price' => 'required',
            'product.stock'  => 'required',
            'product.category'  => 'required',
            
        ]);
        if ($validate->fails())
        {
            $message = $validate->errors()->getMessages();

            return response()->json([
                'success' => false,
                'messages' => $message 
            ], 200);
        }
        $product = [
            'name' => $request->product['name'],
            'price' => $request->product['price'],
            'stock' => $request->product['stock'],
            'category' => $request->product['category']
        ];
        Product::where('id', $request->product['id'])->where('user_id', $request->user_id)->update($product);

        return response()->json(['success' => true, 'message' => 'Product Updated!', 'product' => $product], 200);
    }
}
