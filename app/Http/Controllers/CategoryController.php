<?php

namespace App\Http\Controllers;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;

use function PHPSTORM_META\map;

class CategoryController extends Controller
{
    /**
     * Create a new CategoryController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwtauth');
    }

    public function get(Request $request) {
        return [
            'categories' => Category::where('user_id', $request->user_id)->paginate(5),
            'categories1' => Category::where('user_id', $request->user_id)->get(),
        ];
    }

    public function total(Request $request) {
        return response()->json(['total_categories' => Category::all()->where('user_id', $request->user_id)->count()]);
    }

    public function store(Request $request) {
        
        $validate = Validator::make($request->all(), [
            'category.name'  => 'required',
            
        ]);
        if ($validate->fails())
        {
            $message = $validate->errors()->getMessages();

            return response()->json([
                'success' => false,
                'messages' => $message 
            ], 200);
        }
        $category = Category::create([
            'user_id' => $request->user_id,
            'name' => $request->category['name'],
        ]);

        return response()->json(['success' => true, 'message' => 'Category Added!', 'category' => $category], 200);
    }

    public function delete(Request $request) {
        Category::where('id', $request->id)->delete();
        return response()->json([ 'message' => 'Category Deleted!'], 200);
    }

    public function update(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'category.name'  => 'required',
            
        ]);
        if ($validate->fails())
        {
            $message = $validate->errors()->getMessages();

            return response()->json([
                'success' => false,
                'messages' => $message 
            ], 200);
        }
        $category = [
            'name' => $request->category['name'],
        ];
        Category::where('id', $request->category['id'])->update($category);

        return response()->json(['success' => true, 'message' => 'Category Updated!', 'category' => $category], 200);
    }
}
