import Login from '../js/components/views/Login'
import Register from '../js/components/views/Register'
import Dashboard from '../js/components/views/Dashboard'
import Category from '../js/components/views/Category'
import Inventory from '../js/components/views/Inventory'
import Ppmp from '../js/components/views/Ppmp'
import AccountSettings from '../js/components/views/AccountSettings'
import NotFound from '../js/components/views/NotFound'

const routes = [
    {
        path: '*',
        name: 'NotFound',
        component: NotFound
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    },
    {
        path: '/',
        name: 'Dashboard',
        component: Dashboard
    },
    {
        path: '/category',
        name: 'Category',
        component: Category
    },
    {
        path: '/inventory',
        name: 'Inventory',
        component: Inventory
    },
    {
        path: '/ppmp',
        name: 'Ppmp',
        component: Ppmp
    },
    {
        path: '/account-settings',
        name: 'Account Settings',
        component: AccountSettings
    }
]

export default routes
