require("./bootstrap");

import Vue from "vue";
import VueRouter from "vue-router";
import App from "../js/components/App";
import routes from "./router";
import store from "./store";
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

const options = {
    transition: "Vue-Toastification__fade",
    maxToasts: 20,
    newestOnTop: true
};

Vue.component('pagination', require('laravel-vue-pagination'));
Vue.use(Toast, options);
Vue.use(VueRouter);

const router = new VueRouter({
    history: true,
    mode: "history",
    routes
});

new Vue({
    el: "#app",
    router,
    store,
    render: app => app(App)
});
