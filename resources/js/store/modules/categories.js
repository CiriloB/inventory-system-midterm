const state = {
    categories: [],
    categories1: {},
    totalCategories: 0,
    message: "",
    isSuccess: false
};

const actions = {
    async fetchTotalCategories(state) {
        const response = await axios.get(`/api/category/total?user_id=${state.getters.getUser.id}`, {
            headers: {
                Authorization: "Bearer ".concat(state.getters.getToken)
            }
        });
        state.commit("setTotalCategories", response.data);
    },
    async fetchCategories(state, page=1) {
        const response = await axios.get(
            `/api/category/get?user_id=${state.getters.getUser.id}&page=${page}`,
            {
                headers: {
                    Authorization: "Bearer ".concat(state.getters.getToken)
                }
            }
        );
        state.commit("setCategories", response.data.categories);
        state.commit("setCategories1", response.data.categories1);

    },
    async addCategory(state, category) {
        await axios
            .post("/api/category/store", {
                token: state.getters.getToken,
                user_id: state.getters.getUser.id,
                category: category
            })
            .then(res => {
                if (res.data.success)
                    return state.commit("setResponse", res.data);
                state.commit("setResponse", res.data);
            });
    },
    async updateCategory(state, category) {
        await axios
            .put("/api/category/update", {
                token: state.getters.getToken,
                user_id: state.getters.getUser.id,
                category: category
            })
            .then(res => {
                if (res.data.success)
                    return state.commit("setResponse", res.data);
                state.commit("setResponse", res.data);
            });
    },
    async deleteCategory(state, id) {
        const response = await axios.delete(
            `/api/category/delete?id=${id}&user_id=${state.getters.getUser.id}&token=${state.getters.getToken}`
        );
        state.commit("setMessage", response.data.message);
    }
};

const getters = {
    getCategories: state => state.categories,
    getCategories1: state => state.categories1,
    getTotalCategories: state => state.totalCategories,
    getMessageCategory: state => state.message,
    getSuccessCategory: state => state.isSuccess
};

const mutations = {
    setCategories: (state, categories) => (state.categories = categories),
    setCategories1: (state, categories1) => (state.categories1 = categories1),
    setTotalCategories: (state, category) =>
        (state.totalCategories = category.total_categories),
    setResponse: (state, response) => {
        state.isSuccess = response.success;
        state.message = state.isSuccess ? response.message : response.messages;
    },
    setMessage: (state, message) => (state.message = message),
    clearMessage: state => (state.message = "")
};

export default {
    state,
    getters,
    actions,
    mutations
};
