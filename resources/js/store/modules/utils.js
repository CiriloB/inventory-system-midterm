const state = {
    isDisabled: false
};

const getters = {
    getDisabled: state => state.isDisabled
};

const actions = {};

const mutations = {
    setDisabled: (state, boolean) => (state.isDisabled = boolean)
};

export default {
    state,
    getters,
    actions,
    mutations
};
