const state = {
    //Product
    products: [],
    totalProduct: 0,
    outOfStock: 0,
    message: "",
    isSuccess: false,
    isLoading: true,
    isDisabled: false
};

const getters = {
    //Product
    getProducts: state => state.products,
    getTotalProduct: state => state.totalProduct,
    getOutOfStock: state => state.outOfStock,
    getMessageProduct: state => state.message,
    getSuccessProduct: state => state.isSuccess,
};

const actions = {
    //Product
    async fetchTotalProduct(state) {
        const response = await axios.get(`/api/product/total?user_id=${state.getters.getUser.id}`, {
            headers: {
                Authorization: "Bearer ".concat(state.getters.getToken)
            }
        });
        state.commit("setTotalProduct", response.data);
    },
    async fetchOutOfStock(state) {
        const response = await axios.get(`/api/product/out_of_stock?user_id=${state.getters.getUser.id}`, {
            headers: {
                Authorization: "Bearer ".concat(state.getters.getToken)
            }
        });
        state.commit("setOutOfStock", response.data);
    },
    async fetchProducts(state, page = 1) {
        const response = await axios.get( `/api/product/get?user_id=${state.getters.getUser.id}&page=${page}`, {
            headers: {
                Authorization: "Bearer ".concat(state.getters.getToken)
            }
        });
        state.commit("setProducts", response.data);
    },
    async addProduct(state, product) {
        await axios
            .post("/api/product/store", {
                token: state.getters.getToken,
                user_id: state.getters.getUser.id,
                product: product
            })
            .then(res => {
                if (res.data.success)
                    return state.commit("setResponse", res.data);
                state.commit("setResponse", res.data);
            });
    },
    async updateProduct(state, product) {
        await axios
            .put("/api/product/update", {
                token: state.getters.getToken,
                user_id: state.getters.getUser.id,
                product: product
            })
            .then(res => {
                if (res.data.success)
                    return state.commit("setResponse", res.data);
                state.commit("setResponse", res.data);
            });
    },
    async deleteProduct(state, id) {
        console.log(id)
        const response = await axios.delete(
            `/api/product/delete?id=${id}&user_id=${state.getters.getUser.id}&token=${state.getters.getToken}`
        );
        state.commit("setMessage", response.data.message);
        
    }
};
const mutations = {
    //Product
    setProducts: (state, products) => (state.products = products),
    setTotalProduct: (state, product) =>
        (state.totalProduct = product.total_products),
    setOutOfStock: (state, product) =>
        (state.outOfStock = product.out_of_stock),

    //Message
    setResponse: (state, response) => {
        state.isSuccess = response.success;
        state.message = state.isSuccess ? response.message : response.messages;
    },
    setMessage: (state, message) => (state.message = message),
    clearMessage: state => (state.message = ""),
};

export default {
    state,
    getters,
    mutations,
    actions
};
