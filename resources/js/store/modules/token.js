const state = {
    token: localStorage.getItem("access_token") || ""
};

const getters = {
    getToken: state => state.token
};

const actions = {
    
};

const mutations = {
    //Token
    setToken: (state, token) => {
        localStorage.setItem("access_token", token);
        state.token = token;
    },
    clearToken: state => {
        localStorage.removeItem("access_token");
        state.token = "";
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
