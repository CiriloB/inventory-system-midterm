const state = {
    user: []
};

const getters = {
    getUser: state => state.user
};

const actions = {
    async fetchUser(state) {
        const response = await axios.get("/api/auth/user", {
            headers: {
                Authorization: "Bearer ".concat(state.getters.getToken)
            }
        });
        state.commit("setUser", response.data);
    }
};

const mutations = {
    setUser: (state, user) => (state.user = user)
};

export default {
    state,
    getters,
    actions,
    mutations
};
