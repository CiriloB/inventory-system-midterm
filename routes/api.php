<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategorytController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Product Api Routes
Route::group([

    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'product'

], function ($router) {
    
    Route::get('get', 'ProductController@get');    
    Route::get('total', 'ProductController@total');
    Route::get('out_of_stock', 'ProductController@outOfStock');
    Route::post('store', 'ProductController@store');
    Route::delete('delete', 'ProductController@delete');
    Route::put('update', 'ProductController@update');
    
});

//Category Api Routes
Route::group([

    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'category'

], function ($router) {
    
    Route::get('get', 'CategoryController@get');    
    Route::get('total', 'CategoryController@total');
    Route::post('store', 'CategoryController@store');
    Route::delete('delete', 'CategoryController@delete');
    Route::put('update', 'CategoryController@update');
    
});

//Auth Api Routes
Route::group([

    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'auth'

], function ($router) {
    
    Route::post('register', 'AuthController@register');
    Route::post('verifyToken', 'AuthController@verifyToken');
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('user', 'AuthController@user');

});